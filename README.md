Groupe : NotFound

Membres :

WEBER Guillaume
RUHLMANN Benjamin
BARRE David
LATEVE-HOUSSEMAND Gauthier

LP CISIIE - LP2

Lien du bitbucket : https://bitbucket.org/notfound/findyourway

--------------------------------------------------------

Notre vision du sujet du projet :

Dans le cadre de notre projet, il va s'agir d'administrateurs qui vont être à la charge de création de jeux. Ces jeux seront donc prédéfinis par cet administrateur. Il aura accès à un onglet "administration de jeux". Il va ainsi pouvoir d'abord créer des jeux. Une fois un jeu créé, il va pouvoir lui affecter au total 5 lieux qui seront à deviner par un joueur. Ces lieux sont devinés par des indices que l'administrateur aura définit. En plus des 5 lieux, l'administrateur va devoir définir une destination finale qui sera définit par 5 indices (dans le cas où le joueur devine les 5 lieux : un indice par lieu découvert).

Cette visualisation du projet a rendu l'utilisation des tokens inutile puisqu'une partie est identitié par un unique id lors de sa création par l'admin, de plus elle ne peux pas être modifié par un autre joueur car elle ne dispose pas d'informations spécifiques à stocker quand la partie est en cours de jeu (cela aurait pu être le cas lors de l'envoi d'un pseudo ou d'un score par exemple).

-------------------------------------------------------

Ce projet a été réalisé par le biais d'un serveur wildfly associé à une base de données postgres (déployé sous docker).

Cette base de données est vide, elle se remplie lors de la création de games par l'administrateurs. La page d'administration de jeux, de lieux et d'indices vont donc vous permettre d'alimenter cette base de données

Vous trouverez un dossier se nommant Docker permettant des deployers des images Docker. Nous en avons fait l'utilisation pour postgres. Vous aurez donc un dossier "postgres" contenant les fichiers nécessaires pour ce déployement : le dockerfile, la version de driver ainsi que le entrypoint
Voici la commande servant à créer un utilisateur dans la base de données avec docker : docker run --name "bd_name" -e POSTGRES_USER="user" -e POSTGRES_PASSWORD="password" -p 5432:5432 -d postgres"

Dans wildfly, pour associer la base de données, il vous faudra créer un admin en utilisant le script suivant : add-user.sh

Une fois cet utilisateur crée, vous serez en mesure de paramétrer votre serveur wildfly (localhost:9990 par défaut) en lui associant sa base de donnée postgres.
Une fois connecté sur l'interface de wildfly (http://localhost:9990/), voici ce qu'il faut faire :

Cliquez l'onglet Deployments, puis cliquer sur le bouton "add", choisir de upload un nouveau déploiement puis choisir le driver postgresql

Une fois ceci fait il vous faudra configurer la base de données
Allez dans l'onglet configuration > subsystems > datasources > Non-XA  et ajoutez une datasource
Choisissez une postgresql datasource, et donnez lui le nom PostgresDS, avec un JNDI name : java:/PostgresDS, faites next et ensuite choisissez le driver postgres.jar dans les drivers detectés, next puis associez une connection url à jdbc:postgresql://localhost:5432/postgres, en indiquant vos identifiants utilisés pour postgres

Une fois terminé, ce sera cette datasource là qui sera utilisé dans notre projet java, spécifié dans le persistence.xml



Voici la structure du projet :

Location des packages du projet BACKEND codé en Java :

- /src/main/java

Location du projet FRONTEND javascript :

- /src/main/webapp

Ce dossier va contenir :

- Un fichier index.html qui est le fichier d'appel de l'application
- Un dossier /app contenant le fichier app.js qui est le fichier principal de notre application angularJS
- Un dossier /templates qui va contenir toutes les templates de l'application

L'API a été déployé sur le port 8080 du localhost, voici le lien utilisé :
http://localhost:8080/FindYourWay/api'


-------------------------------------------------------

Stories et URI utilisés dans l'API:

- En tant qu'admin ou joueur, je souhaite voir la liste des jeux qui ont été créés :
METHOD GET : http://localhost:8080/FindYourWay/api/games
    - Params : Aucun paramètre supplémentaire requis

- En tant qu'admin je souhaite créer un jeu :
METHOD POST : http://localhost:8080/FindYourWay/api/games
    - Params : {"name": "..."}

- En tant qu'admin ou joueur, je souhaite voir le détail concernant un jeu précis :
METHOD GET : http://localhost:8080/FindYourWay/api/games/:game_id
    - Params : Aucun paramètre supplémentaire requis

- En tant qu'admin, je souhaite effectuer la suppression d'un jeu précis :
METHOD DELETE : http://localhost:8080/FindYourWay/api/games/:game_id
    - Params : Aucun paramètre supplémentaire requis

- En tant qu'admin, je souhaite voir la liste des lieux d'un jeu :
METHOD GET : http://localhost:8080/FindYourWay/api/games/:game_id/places
    - Params : Aucun paramètre supplémentaire requis

- En tant qu'admin, je souhaite ajouter un lieu d'un jeu :
METHOD POST : http://localhost:8080/FindYourWay/api/games/:game_id/places
    - Params : {"name": "...", "latitude": "...", "longitude" : "...", "myclue" : "..."}

      (latitude et longitude étant des doubles)

- En tant qu'admin, je souhaite voir la "destination finale" d'un jeu :
METHOD GET : http://localhost:8080/FindYourWay/api/games/:game_id/lastPlace
    - Params : Aucun paramètre supplémentaire requis

- En tant qu'admin, je souhaite ajouter un lieu final à un jeu :
METHOD POST : http://localhost:8080/FindYourWay/api/games/:game_id/lastPlace
    - Params : {"name": "...", "latitude": "...", "longitude" : "..."}

- En tant qu'admin, je souhaite voir la liste des indices qui ont été indiqués dans la destination finale:
METHOD GET : http://localhost:8080/FindYourWay/api/games/:game_id/lastPlace/:lastPlace_id/clues
    - Params : Aucun paramètre supplémentaire requis

- En tant qu'admin, je souhaite ajouter un indice à la destination finale:
METHOD POST : http://localhost:8080/FindYourWay/api/games/:game_id/lastPlace/:lastPlace_id/clues
    - Params : {"description": "..."}
