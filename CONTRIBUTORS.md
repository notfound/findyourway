
Contributions du projet :

Benjamin Ruhlmann :

-Développement des règles lors de la réalisation du jeu
-Gestion/interaction avec la carte leaflet
-Création des ressources, représentations et entités concernant "Game"
-Debug et aides à la réalisation de l'API


  Guillaume Weber :

-Réalisation de la page d'administration : ajout/suppression/modification de jeux
-Réalisation de la page d'édition d'un jeu : ajout/suppression de lieux & indices associés à un jeu
-Gestion des indices et de la destination finale sur la réalisation d'un jeu
-Définition des uri de l'API + aide à la réalisation avec Gauthier


  Gauthier Lateve  :

-Travaux sur l'API : réalisation des entités, des représentations et des resources concernant "Clue" & "Place"
-Ajout d'une destination finale sur la page d'édition d'un jeu
-Mise en place de diverses vérifications & sécurités dans les pages d'administrations
-Gestion des données


  David Barre :

-Style css
 
