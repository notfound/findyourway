package entity;

import com.fasterxml.jackson.annotation.JsonBackReference;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.Transient;
import javax.xml.bind.annotation.XmlElement;

@Entity
public class Clue implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    private String id;
    private String description;
    
    @ManyToOne
    @JsonBackReference
    private Place place;
            
    @XmlElement(name="_links")
    @Transient 
    private List<Link> links = new ArrayList<>();
    
    public List<Link> getLinks(){
        return links;
    }
    
     public void addLink(String uri, String rel){
        this.links.add(new Link(rel, uri));
    }

    public void setLinks(List<Link> links) {
        this.links = links;
    }
    
    public Clue() {

    }

    public Clue(String description) {
        this.description = description;
    }

    public Clue(String description, Place place) {
        this.description = description;
        this.place = place;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }    
    
    
    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Place getPlace() {
        return place;
    }

    public void setPlace(Place place) {
        this.place = place;
    }

}
