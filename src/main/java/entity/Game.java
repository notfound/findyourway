package entity;

import com.fasterxml.jackson.annotation.JsonManagedReference;
import io.jsonwebtoken.Jwts;
import java.io.Serializable;
import java.security.Key;
import java.util.ArrayList;
import java.util.List;
import javax.crypto.spec.SecretKeySpec;
import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Transient;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author ben57
 */
@Entity
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Game.findAll", query = "SELECT g FROM Game g")
})
public class Game implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    private String id;
    private String token;
    private String name;
    @OneToMany(cascade = CascadeType.ALL, mappedBy ="game")
    @JsonManagedReference
    private List<Place> places;
    private String lastPlace_id;

    public String getLastPlace() {
        return lastPlace_id;
    }

    public void setLastPlace(String lastPlace) {
        this.lastPlace_id = lastPlace;
    }

    public Game() {
        this.places = new ArrayList<Place>();
    }

    public Game(String id, String name, List<Place> places, String lp) {
        this.id = id;
        this.token="youpi";
        this.name = name;
        this.places = places;
        this.lastPlace_id = lp;
    }

    @XmlElement(name="_links")
    @Transient 
     private List<Link> links = new ArrayList<>();
    
    public List<Link> getLinks(){
        return links;
    }
    
     public void addLink(String uri, String rel){
        this.links.add(new Link(rel, uri));
    }

    public void setLinks(List<Link> links) {
        this.links = links;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }
    
    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }
    
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<Place> getPlaces() {
        return places;
    }

    public void setPlaces(List<Place> places) {
        this.places = places;
    }    

}
