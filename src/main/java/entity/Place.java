/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entity;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonManagedReference;
import control.KeyGenerator;
import java.io.Serializable;
import java.security.Key;
import java.util.ArrayList;
import java.util.List;
import javax.inject.Inject;
import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Transient;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;


@Entity
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Place.findAll", query = "SELECT p FROM Place p")
})

public class Place implements Serializable {
    
    //@Inject
    //private KeyGenerator keygenerator;
     
    @Id
    private String id;
    //private Key token;


    private String name;
    private double latitude;
    private double longitude;
    private String myclue;

    public String getMyclue() {
        return myclue;
    }

    public void setMyclue(String myclue) {
        this.myclue = myclue;
    }
    
    @ManyToOne
    @JsonBackReference
    private Game game;
    
    @ManyToOne
    private Game gameLastPlace;

    public Game getGameLastPlace() {
        return gameLastPlace;
    }

    public void setGameLastPlace(Game gameLastPlace) {
        this.gameLastPlace = gameLastPlace;
    }
    
    
    public Game getGame() {
        return game;
    }

    public void setGame(Game game) {
        this.game = game;
    }
    
    @OneToMany(cascade = CascadeType.ALL, mappedBy ="place")
    @JsonManagedReference
    private List<Clue> clues;

    @XmlElement(name="_links")

    @Transient 
    private List<Link> links = new ArrayList<>();
    
    public List<Link> getLinks(){
        return links;
    }
    
    public void addLink(String uri, String rel){
        this.links.add(new Link(rel, uri));
    }

    public void setLinks(List<Link> links) {
        this.links = links;
    }
    
    public Place(String name, double latitude, double longitude, String myClue) {
        this.name = name;
        this.latitude = latitude;
        this.longitude = longitude;
        this.clues = new ArrayList<Clue>();
        this.myclue = myclue;
    }
    
    public Place(String id, String name, double lati, double longi){
        this.name= name;
        this.id=id;
        this.latitude= lati;
        this.longitude=longi;
        
    }
    
    public Place(){
        this.clues = new ArrayList<Clue>();
    }
    
    
    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public double getLatitude() {
        return latitude;
    }

    public void setLatitude(double latitude) {
        this.latitude = latitude;
    }

    public double getLongitude() {
        return longitude;
    }

    public void setLongitude(double longitude) {
        this.longitude = longitude;
    }

    public List<Clue> getClues() {
        return clues;
    }

    public void setClues(List<Clue> clues) {
        this.clues = clues;
    }   
    
   
}
