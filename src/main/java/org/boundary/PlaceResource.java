/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.boundary;

import entity.Game;
import entity.Place;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;
import javax.ejb.Stateless;
import javax.persistence.CacheStoreMode;
import javax.persistence.EntityManager;
import javax.persistence.EntityNotFoundException;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

/**
 *
 * @author Leopold
 */
@Stateless
public class PlaceResource {
    
    @PersistenceContext
    EntityManager em;
//    
    public Place findById(String id){
        return this.em.find(Place.class, id);
    }
  
    public List<Place> findAll() {
        Query q = this.em.createNamedQuery("Place.findAll", Place.class);
        q.setHint("javax.persistence.cache.storeMode", CacheStoreMode.REFRESH);
        return q.getResultList();
    }
//    
    public Place save(Place cat) {
        cat.setId(UUID.randomUUID().toString());
        cat.setClues(new ArrayList<>());
        
        System.out.println(cat.getMyclue());
        return this.em.merge(cat);
    }
//    
    public void delete(String id) {
        try {
            Place ref = this.em.getReference(Place.class, id);
            this.em.remove(ref);
        } catch (EntityNotFoundException e) {
            // on veut supprimer, et elle n'existe pas, donc c'est bon
        }
    }
    
    public Place addPlaceForGame(String id, Place place){
       Place p = new Place(place.getName(), place.getLatitude(),  place.getLongitude(), place.getMyclue());
       p.setId(UUID.randomUUID().toString());
       p.setMyclue(place.getMyclue());
       p.setGame(this.em.find(Game.class, id));
       p.setMyclue(place.getMyclue());
       this.em.persist(p);
       return p;
    }
    
    public Place addLastPlaceForGame(String id, Place place){
        Place  p = new Place(place.getName(), place.getLatitude(), place.getLongitude(), null);
        p.setId(UUID.randomUUID().toString());
        Game g = this.em.find(Game.class, id);
        g.setLastPlace(p.getId());
        this.em.persist(p);
        return p ;
    }
    
  
    
    public List<Place> findPlacesForGame(String id){
        Query query = this.em.createQuery("SELECT p FROM Place p where p.game.id=:id ");
        query.setParameter("id", id);
        List<Place> liste = query.getResultList();
        return liste;
    }
}
