/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.boundary;

import entity.Clue;
import entity.Place;
import java.util.List;
import java.util.UUID;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.EntityNotFoundException;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

/**
 *
 * @author Guillaume
 */

@Stateless
public class ClueResource {
        @PersistenceContext
        EntityManager em;
        
        public Clue findById(String id){
            return this.em.find(Clue.class, id);
        }
        
        public Clue ajouterClue(String placeId, Clue clue){
            Clue c = new Clue(clue.getDescription());            
             c.setId(UUID.randomUUID().toString());
             c.setPlace(this.em.find(Place.class, placeId ));
              this.em.persist(c);
            return c;
        }
        
           public void delete(String id) {
        try {
            Clue ref = this.em.getReference(Clue.class, id);
            this.em.remove(ref);
        } catch (EntityNotFoundException e) {
            // on veut supprimer, et elle n'existe pas, donc c'est bon
        }
    }
           
            public List<Clue> findCluesForPlace(String id){
        
           Query query = em.createQuery("SELECT c FROM Clue c where c.place.id=:id ");
            query.setParameter("id", id);

            List<Clue> liste = query.getResultList(); // renvoi tous les commentaires associ�s � ce message ici
            
            return liste;
            }
            
            
               public Clue save(Clue msg) { // On a recu de la part du client un json qui est un message, sert faire persister un message
                     msg.setId(UUID.randomUUID().toString()); //uniqid
                     return this.em.merge(msg); // on demande � entity manager de faire persister ce message

                }
        
        
}
