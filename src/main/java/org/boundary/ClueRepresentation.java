/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.boundary;

import entity.Clue;
import java.net.URI;
import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriInfo;

@Path("/clues") // on va mettre une collection, donc nom au pluriel
@Consumes(MediaType.APPLICATION_JSON) // on va consommer et produire du json
@Produces(MediaType.APPLICATION_JSON)
@Stateless // c'estun ejb

public class ClueRepresentation {
    
    @EJB // @Inject  (fonctionne de la m�me mani�re)
    ClueResource clueResource; // on va r�cup�rer une instance de msg ressource
    
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    @Path("/{clueId}")
    public Response getClues(@PathParam("clueId") String clueId) {
        Clue i = this.clueResource.findById(clueId);
        if (i != null) {
            return Response.ok(i).build();
        } else {
            return Response.status(Response.Status.NOT_FOUND).build();
        }
    }
    
    @POST
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public Response addClue(Clue Clue, @QueryParam("placeId") String placeId, @Context UriInfo uriInfo) {
        Clue i = this.clueResource.ajouterClue(placeId, new Clue(Clue.getDescription()));
        URI uri = uriInfo.getBaseUriBuilder()
                .path(PlaceRepresentation.class)
                .path(placeId)
                .path(ClueRepresentation.class)
                .path(i.getId())
                .build();
        return Response.created(uri).entity(i).build();
    }
    
    @DELETE
    @Path("/{clueId}")
    public void deletePlace(@PathParam("clueId") String id) {
        this.clueResource.delete(id);
    }
    
   
    

  
}
