/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.boundary;

import entity.Clue;
import entity.Place;
import java.net.URI;
import java.util.ArrayList;
import java.util.List;
import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.GenericEntity;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriInfo;

/**
 *
 * @author Leopold
 */
 @Path("/places")
 @Consumes(MediaType.APPLICATION_JSON)
@Produces(MediaType.APPLICATION_JSON)
@Stateless
public class PlaceRepresentation {
            
        @EJB
        PlaceResource placeResource;
        @EJB
        ClueResource clueResource;
        
        @GET
        public Response getPlaces(@Context UriInfo uriInfo){
            List <Place> liste = this.placeResource.findAll();
            liste.stream().forEach(p->{
                p.setClues(this.clueResource.findCluesForPlace(p.getId()));
                List<Clue> li = p.getClues();
                li.stream().forEach(c->{
                      c.getLinks().clear();
                      c.addLink(this.getUriForSelfClue(uriInfo, c, p), "self");
                });
                p.getLinks().clear();
                p.addLink(this.getUriForSelfPlace(uriInfo, p), "self");
            });
            GenericEntity<List<Place>> list = new GenericEntity<List<Place>>(liste) {
            };
             return Response.ok(list, MediaType.APPLICATION_JSON).build();
        }
        
        @GET
        @Produces(MediaType.APPLICATION_JSON)
        @Path("/{placeId}")
         public Response getPlace(@PathParam("placeId") String placeId){
             Place p = this.placeResource.findById(placeId);
             p.setClues(this.clueResource.findCluesForPlace(p.getId()));
             if(p!=null){
                    return Response.ok(p).build();
             }else{
                   return Response.status(Response.Status.NOT_FOUND).build();
             }
         }
         
         
     @GET
    @Path("{placeId}/clues") // sous ressource
    public Response getAllClues(@PathParam("placeId") String placeId, @Context UriInfo uriInfo) {
        List<Clue> lc = this.clueResource.findCluesForPlace(placeId); // on va chercher tous les commentaires pour ce messageId, et on envoi un ok avec la liste
        GenericEntity<List<Clue>> list = new GenericEntity<List<Clue>>(lc) {
        };
        return Response.ok(list, MediaType.APPLICATION_JSON).build();
    }
                
        
        
        @POST
    public Response addPlace(Place Place, @Context UriInfo uriInfo) {
        Place newPlace = this.placeResource.save(Place);
        URI uri = uriInfo.getAbsolutePathBuilder().path(newPlace.getId()).build();
        return Response.created(uri)
                .entity(newPlace)
                .build();
    }
    
    @DELETE
    @Path("/{placeId}")
    public void deletePlace(@PathParam("placeId") String id) {
        this.placeResource.delete(id);
    }
        
        
    private String getUriForSelfPlace(UriInfo uriInfo, Place c){
        String uri = uriInfo.getBaseUriBuilder()
                .path(PlaceRepresentation.class)
                .path(c.getId())
                .build()
                .toString();
        return uri;
    }
    

    private String getUriForPlace(UriInfo uriInfo){
        String uri = uriInfo.getBaseUriBuilder()
                .path(PlaceRepresentation.class)  // va rechercher @path messages
                .build()
                .toString();
        return uri;
    }
    
    // pour un Clue d'une Place
    private String getUriForSelfClue(UriInfo uriInfo, Clue i, Place c){
        String uri = uriInfo.getBaseUriBuilder()
                .path(PlaceRepresentation.class)
                .path(c.getId())
                .path(ClueRepresentation.class)
                .path(i.getId())
                .build()
                .toString();
        return uri;
    }
    
    // pour la collection d'Clue d'une Place
    private String getUriForClue(UriInfo uriInfo, Place c){
        String uri = uriInfo.getBaseUriBuilder()
                .path(PlaceRepresentation.class)
                .path(c.getId())
                .path(ClueRepresentation.class)
                .build()
                .toString();
        return uri;
    }    
        
        
    
    
}
