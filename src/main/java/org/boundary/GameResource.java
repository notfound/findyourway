/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.boundary;

import entity.Game;
import entity.Place;
import java.util.List;
import java.util.UUID;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.EntityNotFoundException;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

/**
 *
 * @author ben57
 */
@Stateless
public class GameResource {
        
        @PersistenceContext
        EntityManager em;
        
        public Game findById(String id){
            return this.em.find(Game.class, id);
        }
        
         public List<Game> findAll() {
            Query query = this.em.createNamedQuery("Game.findAll", Game.class);
            List<Game> liste = query.getResultList(); 
            return liste;
        }
       
         
         public Game save(Game c){
             c.setId(UUID.randomUUID().toString());           
             c.setToken("youpio");
             return this.em.merge(c);
         }

           public void delete(String id) {
                try {
                 Game ref = this.em.getReference(Game.class, id);
                    this.em.remove(ref);
                } catch (EntityNotFoundException e) {
            // on veut supprimer, et elle n'existe pas, donc c'est bon
                }
          }
         
}
