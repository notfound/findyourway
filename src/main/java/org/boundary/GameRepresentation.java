/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.boundary;

import control.KeyGenerator;
import entity.Clue;
import entity.Game;
import entity.Place;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import java.net.URI;
import java.security.Key;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import static javax.ws.rs.core.HttpHeaders.AUTHORIZATION;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriInfo;

/**
 *
 * @author ben57
 */
@Path("/games")
@Consumes(MediaType.APPLICATION_JSON) // on va consommer et produire du json
@Produces(MediaType.APPLICATION_JSON)
@Stateless // c'estun ejb
public class GameRepresentation {
    
    @EJB 
    GameResource GameResource; 
    @EJB
    PlaceResource PlaceResource;
    @EJB
    ClueResource ClueResource;
    
        
    @Inject
    private KeyGenerator keyManagement;

    @Context
    private UriInfo uriInfo;
    
    
    @GET
    public Response getAllGames(@Context UriInfo uriInfo){
        
        
        List <Game> lig = this.GameResource.findAll();
        
        lig.stream().forEach(g->{
            
            g.setPlaces(new ArrayList<Place>());
           
        });
        
        if (lig != null) {
            return Response.ok(lig).build();
        } else {
            return Response.status(Response.Status.NOT_FOUND).build();
        }
    }

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    @Path("/{gameId}")
    public Response getOneGames(@PathParam("gameId") String gameId) {
        Game g = this.GameResource.findById(gameId);
        g.setPlaces(this.PlaceResource.findPlacesForGame(gameId));     
        this.PlaceResource.findPlacesForGame(gameId).stream().forEach(p->{
            p.setClues(this.ClueResource.findCluesForPlace(p.getId()));
        });
        if (g!= null) {            
            return Response.ok(g).build();
        } else {
            return Response.status(Response.Status.NOT_FOUND).build();
        }
    }
    
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    @Path("/{gameId}/lastPlace")
    public Response getLastPlaceForGame(@PathParam("gameId")String gameId){
        Game g = this.GameResource.findById(gameId);
        String lpId = g.getLastPlace();        
        Place lastPlace = this.PlaceResource.findById(lpId);
        lastPlace.setClues(new ArrayList<Clue>());
        return Response.ok(lastPlace).build();
    }
    
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    @Path("/{gameId}/lastPlace/{lastPlaceId}/clues")
    public Response getLastPlaceForGame(@PathParam("gameId")String gameId, @PathParam("lastPlaceId") String lastPlaceId){
        List<Clue> liste = this.ClueResource.findCluesForPlace(lastPlaceId);
        return Response.ok(liste).build();
    }
    
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    @Path("/{gameId}/places")
    public Response getPlacesForGame(@PathParam("gameId") String gameId){
       List<Place> list = this.PlaceResource.findPlacesForGame(gameId);
       list.stream().forEach(p->{
           p.setClues(this.ClueResource.findCluesForPlace(p.getId()));       
       });
       return Response.ok(list).build();
    }
    
//    @POST
//    @Produces(MediaType.APPLICATION_JSON)
//    @Path("/{gameId}/places/{placeId}/clues")
//    public Response addClueForPlace(@PathParam("gameId") String gameId, @PathParam String placeId, Clue clue){
//        
//    }
    
    @DELETE
    @Path("/{gameId}")
    public void deleteGame(@PathParam("gameId") String gameId) {
        GameResource.delete(gameId);
    }

    @POST
    public Response addGame(Game game, @Context UriInfo uriInfo) {
        
        URI uri = uriInfo.getBaseUriBuilder()
                .path(GameRepresentation.class)
                .build();
        String token = issueToken(game.getName());
//        game.setToken(token);
        Game g = this.GameResource.save(game);
          
        return Response.ok(g).build();
    }
    
    @POST
    @Path("/{gameId}/places")
    public Response addPlaces(@PathParam("gameId") String gameId, Place p, @Context UriInfo uriInfo){
            Place place = this.PlaceResource.addPlaceForGame(gameId, p);
              URI uri = uriInfo.getBaseUriBuilder()
                .path(GameRepresentation.class)
                .path(gameId)
                .path(PlaceRepresentation.class)
                .path(place.getId())
                .build();
        return Response.created(uri).entity(place).build();
    }

    @POST
     @Produces(MediaType.APPLICATION_JSON)
    @Path("/{gameId}/lastPlace")
    public Response addLastPlace(@PathParam("gameId") String gameId, Place p, @Context UriInfo uriInfo){
       Place place=  this.PlaceResource.addLastPlaceForGame(gameId, p);
        URI uri = uriInfo.getBaseUriBuilder()
                .path(GameRepresentation.class)
                .path(gameId)
                .path(PlaceRepresentation.class)
                .path(place.getId())
                .build();
        return Response.created(uri).entity(place).build();
    }
    
    @POST
    @Path("/{gameId}/lastPlace/{placeId}/clues")
    public Response addClueForLastPlace(@PathParam("gameId")String gameId, @PathParam("placeId") String placeId, Clue clue, @Context UriInfo uriInfo){
        Clue c = this.ClueResource.ajouterClue(placeId, clue);
         URI uri = uriInfo.getBaseUriBuilder()
                .path(GameRepresentation.class)
                .path(gameId)
                .path(PlaceRepresentation.class)
                .path(placeId)
                 .path(ClueRepresentation.class)
                .path(c.getId())                 
                .build();
         return Response.created(uri).entity(c).build();
    }
    
     public String issueToken(String n){
        Key key = keyManagement.generateKey();
         String jwtToken = Jwts.builder()
                 .setSubject(n)
                .setIssuer(uriInfo.getAbsolutePath().toString())
                .setIssuedAt(new Date())
                .setExpiration(toDate(LocalDateTime.now().plusMinutes(5L)))
                .signWith(SignatureAlgorithm.HS512, key)
                .compact();
          return jwtToken;
    }

    private Date toDate(LocalDateTime localDateTime) {
         return Date.from(localDateTime.atZone(ZoneId.systemDefault()).toInstant());
    }



}
