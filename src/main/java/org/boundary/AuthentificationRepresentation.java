/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.boundary;

import control.KeyGenerator;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import java.security.Key;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.Date;
import javax.inject.Inject;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import static javax.ws.rs.core.HttpHeaders.AUTHORIZATION;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriInfo;

/**
 *
 * @author Leopold
 */

@Path("/authentification")
public class AuthentificationRepresentation {
      
    @Inject
    private KeyGenerator keyManagement;

    @Context
    private UriInfo uriInfo;
    
    @GET
    @Produces("application/json")
    @Consumes("application/json")
    public Response creationToken(){
        String token = issueToken();
          return Response.ok().header(AUTHORIZATION, "Bearer " + token).build();
    }
    
    public String issueToken(){
        Key key = keyManagement.generateKey();
         String jwtToken = Jwts.builder()
                .setIssuer(uriInfo.getAbsolutePath().toString())
                .setIssuedAt(new Date())
                .setExpiration(toDate(LocalDateTime.now().plusMinutes(5L)))
                .signWith(SignatureAlgorithm.HS512, key)
                .compact();
          return jwtToken;
    }

    private Date toDate(LocalDateTime localDateTime) {
         return Date.from(localDateTime.atZone(ZoneId.systemDefault()).toInstant());
    }
}
