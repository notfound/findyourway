var app = angular.module("findyourway", ['ngResource', 'ngRoute']);

app.constant('api', {'url':'http://localhost:8080/FindYourWay/api'});

app.config(['api', '$routeProvider', function(api, $routeProvider){

    // Routes

    $routeProvider.when('/', {
      templateUrl: 'templates/home.html',
      controller: 'homeController'
    })
                  .when('/game/:id', {
      templateUrl: 'templates/game.html',
      controller: 'gameController'
    })
                  .when('/admin', {
      templateUrl : 'templates/admin.html',
      controller: 'adminController'
    })
                  .when('/EditGame/:id', {
      templateUrl: 'templates/editgame.html',
      controller: 'editGameController'
    })
                  .when('/gameList', {
      templateUrl : 'templates/gamelist.html',
      controller : 'gameListController'
    });
}]);


// Factory

// Places

//Factory concernant les lieux

app.factory("Place", ['$resource', 'api', function($resource, api){
  return $resource(api.url+ "/games/:game_id/places/:id", {game_id: 'game_id'}, {
     save_place : {method: 'POST', url: api.url + "/games/:id/places"},
     delete_place : {method: 'DELETE', url: api.url + '/places/:place_id'},
     saveLastPlace : {method: 'POST', url: api.url+'/games/:id/lastPlace'}
  });
}]);

app.factory("lastPlace", ['$resource', 'api', function($resource, api){
  return $resource(api.url+ "/games/:game_id/lastPlace/:lastPlace_id/:lastPlace/clues", {game_id: 'game_id', lastPlace_id: 'lastPlace_id'}, {
     saveLastPlace : {method: 'POST', url: api.url+'/games/:id/lastPlace'},
     getLastPlace : {method: 'GET', url: api.url+'/games/:game_id/lastPlace'}
  });
}]);


// Factory concernant les clues

app.factory("Clue", ['$resource', 'api', function($resource, api){
  return $resource(api.url+ "/games/:game_id/lastPlace/:place_id/clues", {place_id: 'place_id', game_id: 'game_id'}, {
     save_clue : {method : 'POST', url: api.url + "/games/:game_id/lastPlace/:place_id/clues"}
  });
}]);

// Factory concernant les games

app.factory("Game", ['$resource', 'api', function($resource, api){
  return $resource(api.url+ "/games/:id", {id: '@_id'}, {
  });
}]);

app.controller("homeController", ['$scope', 'Game', '$location', function($scope, Game, $location){

  $scope.gameList = function(){
    $location.path("/gameList");
  }

}]);

app.run(['$rootScope', '$location', function($rootScope, $location){
$rootScope.home = function(){
  $location.path('/');
}
}]);

app.controller("adminController", ['$scope', 'Game', 'Place', '$location', '$route', function($scope, Game, Place, $location, $route){
  $scope.games = Game.query();
  console.log($scope.games);

  $scope.createGame = function(){
    $scope.game = new Game({
      name : $scope.name
    });

    console.log($scope.name);

    $scope.game.$save(function(g){
	     $scope.games.push(g);
    }, function(error){
      console.log(error);
    });
  }
  $scope.delete_game = function(g){
    Game.delete({id: g.id}, function(){
      $scope.games.splice($scope.games.indexOf(g), 1);
    },function(error){
      console.log(error);
    });
  }

}]);

app.controller("editGameController", ['$scope', 'Game', 'Place', 'Clue', 'lastPlace', '$routeParams', '$location', '$route', function($scope, Game, Place, Clue, lastPlace, $routeParams, $location, $route){

  var mymap = L.map('mapid').setView([46.8, 1.7], 5);
  L.tileLayer('https://api.mapbox.com/styles/v1/lidrel/ciyn0w5jb009z2skejaramrwh/tiles/256/{z}/{x}/{y}?access_token=pk.eyJ1IjoibGlkcmVsIiwiYSI6ImNpeW4wc2E3YjAwMG8zMXBnbTg2Zzc4dTkifQ.wUoz-nwYSH2QYRBbS1r7_g')
    .addTo(mymap);

  function onMapClick(e) {
      L.marker([e.latlng['lat'], e.latlng['lng']]).addTo(mymap);

      $scope.game = Game.get({id: $routeParams.id}, function(success){
        $scope.longitude = e.latlng['lat'];
        $scope.latitude = e.latlng['lng'];
      }, function(error){
        console.log(error);
      });

  }
  mymap.on('click', onMapClick);

  // console.log(e.latlng['lat]);

  $scope.createGame = function(){
    $scope.game = new Game({
      name : $scope.name
    });
  }

  var idLastPlace= "";
  $scope.places = Place.query({game_id : $routeParams.id});
  $scope.lastPlace = lastPlace.getLastPlace({game_id : $routeParams.id});

  Promise.resolve($scope.lastPlace.$promise).then(function(valeur) {
    idLastPlace = valeur.id;
    console.log(idLastPlace);
    $scope.clues = Clue.query({place_id: idLastPlace, game_id : $routeParams.id});
    Promise.resolve($scope.clues.$promise).then(function(valeur) {
      if(valeur.length!=0){
        $(".btnAjouterClues").remove();
        for(var i=0; i<valeur.length; i++){
          $(".cluesTd").append(valeur[i].description+" ;");
        }
      }
    });
  });


  $scope.createLastPlace =function(){
      lastPlace.saveLastPlace({id : $routeParams.id}, {
        name : $scope.lastPlacename,
        longitude : $scope.longitude,
        latitude : $scope.latitude
      }, function(success){
        console.log(success);
        $route.reload();
      }, function(error){
        console.log(error);
      });
  }

  $scope.createPlace = function(place){
    if($scope.places.length <= 4 ){
      Place.save_place({id : $routeParams.id}, {
        name : $scope.name,
        longitude : $scope.longitude,
        latitude : $scope.latitude,
        myclue : $scope.myclue
      }, function(success){
        $scope.places.push(success);
      },function(error){
        console.log(error);
      });
    }else{
     alert("vous ne pouvez plus ajouter de plus de 5 lieux");
    }
  }



  $scope.saveClues = function(){
    for (var i = 1; i < 6; i++) {
      var temp = 0;
        switch (i){
          case 1 :
          temp = $scope.indice1
          break;
          case 2 :
          temp = $scope.indice2
          break;
          case 3 :
          temp = $scope.indice3
          break;
          case 4 :
          temp = $scope.indice4
          break;
          case 5 :
          temp = $scope.indice5
          break;
        }
        console.log($scope.lastPlace.id);
        Clue.save_clue({place_id: $scope.lastPlace.id, game_id : $routeParams.id}, {
         description : temp
      }, function(success){
          $(".btnAjouterClues").remove();
          console.log(success);
          $(".cluesTd").append(success.description+" ");
      },function(error){
        console.log(error);
      });
    }
  }

  $scope.delete_place = function(p){
      Place.delete_place({place_id: p.id}, function(){
        $scope.places.splice($scope.places.indexOf(p), 1);
      },function(error){
        console.log(error);
      });
  }

}]);

app.controller("gameListController", ['$scope', '$http', 'Game', function($scope, $http, Game){
  $scope.games = Game.query();
  console.log($scope.games);
  $scope.play_game = function(g){
    // $location.path("/game");
  }
}]);

app.controller("gameController", ['$scope', '$http', 'Place', 'lastPlace', '$routeParams', function($scope, $http, Place, lastPlace, $routeParams) {

  var mymap = L.map('mapgame').setView([46.8, 1.7], 5);
  L.tileLayer('https://api.mapbox.com/styles/v1/lidrel/ciyn0w5jb009z2skejaramrwh/tiles/256/{z}/{x}/{y}?access_token=pk.eyJ1IjoibGlkcmVsIiwiYSI6ImNpeW4wc2E3YjAwMG8zMXBnbTg2Zzc4dTkifQ.wUoz-nwYSH2QYRBbS1r7_g')
    .addTo(mymap);

    var point = 0;
    var destination = [0, 0];
    var popup = L.popup();
    var distance = 8000;
    var pt1, pt2;
    var clics = 6;
    var places = [];
    var clues = [];
    var i = 0;
    var i_clues = 0;

    $scope.allplaces = Place.query({game_id: $routeParams.id}, function(allplaces){
      angular.forEach(allplaces, function(value, key){
        places[i] = [value.longitude, value.latitude];
        i ++;
      });
  });


    $scope.last_place = lastPlace.getLastPlace({game_id: $routeParams.id});
    console.log($scope.last_place);
    Promise.resolve($scope.last_place.$promise).then(function(valeur) {
      $scope.last_place_clues = lastPlace.query({game_id: $routeParams.id, lastPlace_id: valeur.id});
      destination = [valeur.longitude, valeur.latitude];
      console.log(destination);
    });


    var ind = false;

  function onMapClick(e) {
      this.ind = false;
      if(clics > 0){
          L.marker([e.latlng['lat'], e.latlng['lng']]).addTo(mymap);
          pt2 = e.latlng;
          point = calculDistance(pt2['lat'], pt2['lng'], destination[0], destination[1]);
          if(point === 0){
              for (var i = 0; i < places.length; i++) {
                  var indice = calculDistance(pt2['lat'], pt2['lng'], places[i][0], places[i][1]);
                  if(indice > 0){
                      this.ind = true;
                      //devoilerIndice(places[i]);
                  }
              }
          }
          console.log(e.latlng['lat'] + " "+e.latlng['lng']);
          if(typeof pt1 !== "undefined" && typeof pt2 !== "undefined"){
              drawLine(pt1, pt2);
          }
          pt1 = e.latlng;

          //POPUPS
          if(point > 0){
              popup
              .setLatLng(e.latlng)
              .setContent("FELICITATION, vous avez TROUVE le lieu final ! Vous obtenez "+point+" points !")
              .openOn(mymap);
              mymap.off('click', onMapClick);
          }else{
              if(clics>0 && !this.ind){
                  popup
                  .setLatLng(e.latlng)
                  .setContent("LOUPE ! ;)")
                  .openOn(mymap);
              }else if(clics>0 && this.ind){
                  popup
                  .setLatLng(e.latlng)
                  .setContent("Vous avez TROUVE un indice !")
                  .openOn(mymap);
                  Promise.resolve($scope.last_place_clues).then(function(valeur) {
                    console.log(valeur[i_clues].description);
                  $("#indice").append("<tr><td>"+valeur[i_clues].description+"</td></tr>");
                  i_clues++;
                });
              }else{
                  alert("vous avez PERDU  !");
                  event.stopPropagation();
              }
          }
          console.log("clics restant : "+clics);
      }else{
          alert("vous avez perdu :(");
      }
      clics--;
  }
      mymap.on('click', onMapClick);

  //Conversion des degrï¿½s en radian
  function convertRad(input){
          return (Math.PI * input)/180;
  }

  function calculDistance(lat_a_degre, lon_a_degre, lat_b_degre, lon_b_degre){
      var pts = 0;
      R = 6378000; //Rayon de la terre en mï¿½tre

      lat_a = convertRad(lat_a_degre);
      lon_a = convertRad(lon_a_degre);
      lat_b = convertRad(lat_b_degre);
      lon_b = convertRad(lon_b_degre);

      d = R * (Math.PI/2 - Math.asin( Math.sin(lat_b) * Math.sin(lat_a) + Math.cos(lon_b - lon_a) * Math.cos(lat_b) * Math.cos(lat_a)));
      console.log(d);
      if(d < distance){
          pts = 10;
      }else if(d > distance && d < 2*distance){
          pts = 8;
      }else if(d > 2*distance && d < 3*distance){
          pts = 6;
      }else if(d > 3*distance && d < 5*distance){
          pts = 3;
      }else if(d > 5* distance && d < 10*distance){
          pts = 1;
      }
      return pts;
  }

  function drawLine(point1, point2){
      var polyline = L.polyline([point1, point2], {color : "blue"}).addTo(mymap);
  }

$scope.places = Place.query({game_id: $routeParams.id});



}]);
